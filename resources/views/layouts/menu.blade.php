<div class="position-sticky pt-3">
    <ul class="nav flex-column">
        <li class="nav-item">
            <a class="nav-link active" aria-current="page" href="#">
                <span data-feather="home"></span>
                Person List
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="add-person">
                <span data-feather="file"></span>
                Add Person
            </a>
        </li>
    </ul>

</div>
