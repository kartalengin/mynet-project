<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Person extends Model
{
    use HasFactory;

    protected $fillable = [
        'name', 'birthday', 'gender','address_id','address'
    ];

    protected $table = "Person";

    public function address()
    {
        return $this->hasOne(Address::class,'id','address_id');
    }
}
